extern crate gcc;

use std::process::Command;
use std::path::Path;

fn main() {
    Command::new("make")
        .args(&["hotptp"])
        .current_dir(Path::new("src/"))
        .status().unwrap();
    gcc::Config::new()
        .file("src/lex.yy.c")
        .file("src/y.tab.c")
        .file("service.c")
        .define("P_USERPROC", Some("1"))
        .compile("libfbtptpparser.a");
}
