void * p_build_tree(char * sym,
                    void * A,
                    void * B,
                    void * C,
                    void * D,
                    void * E,
                    void * F,
                    void * G,
                    void * H,
                    void * I,
                    void * J);

void * p_token(char * tok, int32_t symIdx);
void p_print(void * tree);

#define P_BUILD(sym,A,B,C,D,E,F,G,H,I,J) p_build_tree(sym,A,B,C,D,E,F,G,H,I,J)
#define P_TOKEN(tok,symIdx) p_token(tok,symIdx)

#define P_ACT(ss)
#define P_PRINT(ss) p_accept(ss)
//#define P_ACT(ss) if(verbose)printf("%7d %s\n",yylineno,ss);
//#define P_PRINT(ss) if(verbose){printf("ast(");pPrintTree(ss,0);printf(").\n");}
