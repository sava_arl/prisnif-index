//NOTE: Очень много повторяющегося кода. Минимум в два раза можно сократить повторения.

use def::*;
use std::fmt;

use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

extern crate time;
use self::time::PreciseTime;

//====================================
pub fn fof_to_pcf(f2:FolFormula) -> PCFormula{
    let f = trans(f2);
    //fof_to_jsfile(&f);
    let mut st = Symtable::new();
    let vsbs = VarSymBindingStack::new(None,&Vec::<GTerm>::new(),&mut st);
    let mut sb = SymBinding::new();

    let start = PreciseTime::now();
    let pcf = fof_to_pcf2(f, &mut st, &vsbs, &mut sb);
    let end = PreciseTime::now();
    println!("FOF to PCF: {} seconds.", start.to(end));
    //pcf_to_jsfile(&pcf, &st);


    //println!("{:?}",c.varmap);
    //println!("========");
    //st.print();
    //println!("========");
    //vsbs.print();
    //println!("========");
    //sb.print();
    pcf
}

//из одной формулы делаем список
//pcf = PyB-{QC-{Pz1D1-Fi1, Pz2D2-Fi2...}, Psi}
// pub fn reduct_pcf_x(pcf:PCFormula)->Vec<PCFormula>{
//     match pcf{
//         PCFormula::A(vars,conj,frms) => {
//
//         },
//
//     }
// }
// pub fn reduct_pcf(pcf:PCFormula) -> PCFormula {
//
// }

pub fn regulation(f:PCFormula, q:Quantifier ) -> PCFormula {
    // match (f,q){
    //     (PCFormula::A(vars,conj,f2), Quantifier::Forall) => {
    //         PCFormula::A(vars,conj,f2)
    //     },
    //     (PCFormula::A(vars,conj,f2), Quantifier::Exists) => {
    //         PCFormula::E(Vec::new(),Vec::new(),vec![PCFormula::A(vars,conj,f2)])
    //     },
    //     (PCFormula::E(vars,conj,f2), Quantifier::Exists) => {
    //         PCFormula::E(vars,conj,f2)
    //     },
    //     (PCFormula::E(vars,conj,f2), Quantifier::Forall) => {
    //         PCFormula::A(Vec::new(),Vec::new(),vec![PCFormula::E(vars,conj,f2)])
    //     },
    // }
    f
}

pub fn fof_to_pcf2(f:FolFormula, st:&mut Symtable, vsbs:&VarSymBindingStack, sb:&mut SymBinding) -> PCFormula {
    match f{
        FolFormula::Forall(vars,f2) => {
            let vsbs2 = VarSymBindingStack::new(Some(Box::new(vsbs)),&vars,st);
            let vars2 = create_vec_terms(vars, &vsbs2, st, sb);
            PCFormula::A(vars2,Vec::new(),vec![regulation(fof_to_pcf2(*f2, st, &vsbs2, sb), Quantifier::Exists)])
        },
        FolFormula::Exists(vars,f2) => {
            let vsbs2 = VarSymBindingStack::new(Some(Box::new(vsbs)),&vars,st);
            let vars2 = create_vec_terms(vars, &vsbs2, st, sb);
            PCFormula::E(vars2,Vec::new(),vec![regulation(fof_to_pcf2(*f2, st, &vsbs2, sb), Quantifier::Forall)])
        },
        FolFormula::Or(disj) => {
            let dtr = disj.into_iter()
                            .map(|d| regulation(fof_to_pcf2(d, st, &vsbs, sb),Quantifier::Exists))
                            .collect();
            PCFormula::A(Vec::new(),Vec::new(),dtr)
        },
        FolFormula::And(conj) => {
            let dtr = conj.into_iter()
                            .map(|d| regulation(fof_to_pcf2(d, st, &vsbs, sb),Quantifier::Forall))
                            .collect();
            PCFormula::E(Vec::new(),Vec::new(),dtr)
        },
        FolFormula::Neg(a) => {
            let t = match *a {
                FolFormula::Atom(t2) => create_term(*t2, vsbs, st, sb),
                _ => panic!("fof_to_pcf: Neg"),
            };
            PCFormula::A(Vec::new(),vec![t],Vec::new())
        },
        FolFormula::Atom(t) => {
            let t2 = create_term(*t, vsbs, st, sb);
            PCFormula::E(Vec::new(),vec![t2],Vec::new())
        },
        _ => panic!("fof_to_pcf: _"),
    }
}

pub fn trans(f: FolFormula) -> FolFormula {
    match f {
        FolFormula::Neg(p) => {
            let p1 = *p;
            match p1{
                //~~a = a
                FolFormula::Neg(p2) => {
                    trans(*p2)
                },
                //~(a => b) = (a & ~b)
                FolFormula::Impl(p2,p3) => {
                    FolFormula::And(vec![trans(*p2),trans(FolFormula::Neg(p3))])
                },
                //~(a <=> b) = (~(a => b) | ~(b => a))
                FolFormula::Equiv(p2,p3) => {
                    let p2t = FolFormula::Neg(Box::new(FolFormula::Impl(p2.clone(),p3.clone())));
                    let p3t = FolFormula::Neg(Box::new(FolFormula::Impl(p3.clone(),p2.clone())));
                    FolFormula::Or(vec![trans(p2t),trans(p3t)])
                },
                //~(a & b) = (~a | ~b)
                FolFormula::And(conj) => {
                    FolFormula::Or(conj.into_iter()
                                 .map(|d| trans(FolFormula::Neg(Box::new(d))))
                                 .collect())
                },
                //~(a | b) = (~a & ~b)
                FolFormula::Or(disj) => {
                    FolFormula::And(disj.into_iter()
                                 .map(|d| trans(FolFormula::Neg(Box::new(d))))
                                 .collect())
                },
                //~![vars]F = ?[vars]~F
                FolFormula::Forall(vars,p2) => FolFormula::Exists(vars,Box::new(trans(FolFormula::Neg(p2)))),
                //~?[vars]F = ![vars]~F
                FolFormula::Exists(vars,p2) => FolFormula::Forall(vars,Box::new(trans(FolFormula::Neg(p2)))),
                //~A = ~A
                FolFormula::Atom(_) => FolFormula::Neg(Box::new(p1)),
            }
        },
        FolFormula::Impl(f1,f2) => FolFormula::Or(vec![trans(FolFormula::Neg(f1)),trans(*f2)]),
        FolFormula::Equiv(f1,f2) => {
            let f1t = FolFormula::Or(vec![FolFormula::Neg(f1.clone()),(*f2).clone()]);
            let f2t = FolFormula::Or(vec![FolFormula::Neg(f2.clone()),(*f1).clone()]);
            FolFormula::And(vec![trans(f1t),trans(f2t)])
        },

        FolFormula::And(conj) => {
            FolFormula::And(conj.into_iter()
                            .map(|d| trans(d))
                            .collect())
        },
        FolFormula::Or(disj) => {
            FolFormula::Or(disj.into_iter()
                            .map(|d| trans(d))
                            .collect())
        },
        FolFormula::Forall(vars,f1) => {
            FolFormula::Forall(vars,Box::new(trans(*f1)))
        },
        FolFormula::Exists(vars,f1) => {
            FolFormula::Exists(vars,Box::new(trans(*f1)))
        },
        FolFormula::Atom(_) => f,
    }
}

//NOTE: Разобраться с Formula Role в TPTP. Возможно нужно проверять каждую формулу
//и в зависимости от роли совершать действия. Пока просто отрицаем конъюнкцию.
pub fn problem_to_fof(p:Problem) -> FolFormula {
    let fols = p.ff.into_iter()
                    .map(|af| {
                        af.formula
                    })
                    .collect();
    //NOTE: Если fols.len() == 1, то просро отрицание, без конъюнкции
    //FolFormula::Neg(Box::new(FolFormula::And(fols)))
    FolFormula::And(fols)
}

// ================================================================================================
// ================ JSON ================
// ================================================================================================
pub fn pcf_to_jsfile(pcf:&PCFormula, st:&Symtable){
    let path = Path::new("problems/tree/pcfdata.js");
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           Error::description(&why)),
        Ok(file) => file,
    };

    let pcfstr = pcf_to_json(pcf,st);
    let mut pcfjs = "var data = {'core':{'data':[".to_string();
    pcfjs.push_str(&pcfstr);
    pcfjs.push_str("]}};");

    match file.write_all(pcfjs.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               Error::description(&why))
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn pcf_to_json(pcf:&PCFormula, st:&Symtable) -> String{
    let mut s = String::from("");
    match *pcf{
        PCFormula::A(ref vars, ref conj, ref frms) => {
            s.push_str("{\'text\':\'");
            s.push_str("A[");
            for i in 0..vars.len(){
                let xx = vars[i].to_str(st);
                s.push_str(&xx);
                if i < vars.len()-1 {
                    s.push_str(", ");
                }
            }
            s.push_str("]");
            s.push_str("[");
            for i in 0..conj.len(){
                let xx = conj[i].to_str(st);
                s.push_str(&xx);
                if i < conj.len()-1 {
                    s.push_str(", ");
                }
            }
            s.push_str("]");
            s.push_str("\'");

            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            //children
            s.push_str(",\'children\':[");
            for i in 0..frms.len(){
                let djson = pcf_to_json(&frms[i],st);
                s.push_str(&djson);
                if i < frms.len()-1 {
                    s.push_str(",");
                }
            }
            s.push_str("]}");
        },
        PCFormula::E(ref vars, ref conj, ref frms) => {
            s.push_str("{\'text\':\'");
            s.push_str("E[");
            for i in 0..vars.len(){
                let xx = vars[i].to_str(st);
                s.push_str(&xx);
                if i < vars.len()-1 {
                    s.push_str(", ");
                }
            }
            s.push_str("]");
            s.push_str("[");
            for i in 0..conj.len(){
                let xx = conj[i].to_str(st);
                s.push_str(&xx);
                if i < conj.len()-1 {
                    s.push_str(", ");
                }
            }
            s.push_str("]");
            s.push_str("\'");

            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            //children
            s.push_str(",\'children\':[");
            for i in 0..frms.len(){
                let djson = pcf_to_json(&frms[i],st);
                s.push_str(&djson);
                if i < frms.len()-1 {
                    s.push_str(",");
                }
            }
            s.push_str("]}");
        },
    }
    s
}


pub fn v_to_json(v: &Vec<AnnotatedFolFormula>) -> String{
    let mut s = String::from("");
        for i in 0..v.len(){
            let xx = annfof_to_json(&v[i]);
            s.push_str(&xx);
            if i < v.len()-1{
                s.push_str(",")
            }
        }
    s
}

pub fn annfof_to_json(a:&AnnotatedFolFormula) -> String{
    let mut s = String::from("");
    match *a{
        AnnotatedFolFormula{ref name, ref role, ref formula} => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            let ro = (*role).to_string();
            s.push_str(&ro);
            s.push_str(", ");
            s.push_str(name);
            s.push_str("\'");
            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            s.push_str(",\'children\':[");
            let f1json = fof_to_json(formula);
            s.push_str(&f1json);
            s.push_str("]}");
        }
    }
    s
}

pub fn fof_to_json(fof:&FolFormula) -> String {
    //let mut fofjson = String::from("{");
    let mut s = String::from("");
    match *fof{
        FolFormula::Exists(ref vars,ref formula) => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            s.push_str("Exists[");
            for i in 0..vars.len(){
                let xx = vars[i].to_string();
                s.push_str(&xx);
                if i < vars.len()-1 {
                    s.push_str(", ");
                }
            }
            s.push_str("]:\'");

            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            s.push_str(",\'children\':[");
            let fjson = fof_to_json(&*formula);
            s.push_str(&fjson);
            s.push_str("]}");
        },
        FolFormula::Forall(ref vars,ref formula) => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            s.push_str("Forall[");
            for i in 0..vars.len(){
                let xx = vars[i].to_string();
                s.push_str(&xx);
                if i < vars.len()-1 {
                    s.push_str(", ");
                }
            }
            s.push_str("]:\'");

            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            s.push_str(",\'children\':[");
            let fjson = fof_to_json(&*formula);
            s.push_str(&fjson);
            s.push_str("]}");
        },
        FolFormula::Equiv(ref f1,ref f2) => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            s.push_str("<=>");
            s.push_str("\'");
            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            s.push_str(",\'children\':[");
            let f1json = fof_to_json(&*f1);
            let f2json = fof_to_json(&*f2);
            s.push_str(&f1json);
            s.push_str(",");
            s.push_str(&f2json);
            s.push_str("]}");
        },
        FolFormula::Impl(ref f1,ref f2) => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            s.push_str("=>");
            s.push_str("\'");
            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            s.push_str(",\'children\':[");
            let f1json = fof_to_json(&*f1);
            let f2json = fof_to_json(&*f2);
            s.push_str(&f1json);
            s.push_str(",");
            s.push_str(&f2json);
            s.push_str("]}");
        },
        FolFormula::Or(ref d) => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            s.push_str("Or");
            s.push_str("\'");
            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            s.push_str(",\'children\':[");
            for i in 0..d.len(){
                let djson = fof_to_json(&d[i]);
                s.push_str(&djson);
                if i < d.len()-1 {
                    s.push_str(",");
                }
            }
            s.push_str("]}");
        },
        FolFormula::And(ref d) => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            s.push_str("And");
            s.push_str("\'");
            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            s.push_str(",\'children\':[");
            for i in 0..d.len(){
                let djson = fof_to_json(&d[i]);
                s.push_str(&djson);
                if i < d.len()-1 {
                    s.push_str(",");
                }
            }
            s.push_str("]}");
        },
        FolFormula::Neg(ref formula) => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            s.push_str("Not");
            s.push_str("\'");
            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            s.push_str(",\'children\':[");
            let f1json = fof_to_json(&*formula);
            s.push_str(&f1json);
            s.push_str("]}");
        },
        FolFormula::Atom(ref t) => {
            //node
            //let mut s = String::from("");
            s.push_str("{\'text\':\'");
            let xx = t.to_string();
            s.push_str(&xx);
            s.push_str("\'");
            //state string
            s.push_str(",\'state\':{\'opened\':true,\'selected\':false}");
            //children
            //s.push_str(",\'children\':[");
            //let f1json = fof_to_json(&*formula);
            //s.push_str(&f1json);
            //s.push_str("]}")
            s.push_str("}");
        },
    }
    //fofjson.push_str("}");
    //fofjson
    s
}

pub fn fof_to_jsfile(fof1:&FolFormula){
    let path = Path::new("problems/tree/fofdata.js");
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           Error::description(&why)),
        Ok(file) => file,
    };

    let fofstr = fof_to_json(fof1);
    let mut fofjs = "var data = {'core':{'data':[".to_string();
    fofjs.push_str(&fofstr);
    fofjs.push_str("]}};");

    match file.write_all(fofjs.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               Error::description(&why))
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

pub fn problem_to_jsfile(p:&Problem){
    let start2 = PreciseTime::now();
    vfofs_to_jsfile(&((*p).ff));
    let end2 = PreciseTime::now();
    println!("JSON: {} secods.",start2.to(end2));
}

pub fn vfofs_to_jsfile(vf:&Vec<AnnotatedFolFormula>){
    let path = Path::new("problems/tree/data.js");
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}",
                           display,
                           Error::description(&why)),
        Ok(file) => file,
    };

    let vfofsstr = v_to_json(vf);
    let mut fofjs = "var data = {'core':{'data':[".to_string();
    fofjs.push_str(&vfofsstr);
    fofjs.push_str("]}};");

    match file.write_all(fofjs.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               Error::description(&why))
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }
}
